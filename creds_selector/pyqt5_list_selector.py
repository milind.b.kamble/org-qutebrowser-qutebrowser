#!/usr/bin/env python3

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
import argparse

selection = 0
def parse_args():
    parser = argparse.ArgumentParser(description="Select an item from a list of choices")
    parser.add_argument("--title", "-t", default="List item selector",
                        help="Title string for main window")
    parser.add_argument("-listtype", "-l", default="item",
                        help="Customized label to indicate type of list")
    return parser.parse_args()

class MainWindow(QMainWindow):

    def __init__(self, title, listtype, choices):
        super(MainWindow, self).__init__()

        self.setWindowTitle(title)

        layout = QVBoxLayout()

        widget = QLabel(f"Choose a {listtype} from the following:")
        layout.addWidget(widget)
        
        widget = QListWidget()
        widget.addItems([f"{id+1}: {c}" for (id, c) in enumerate(choices)])

        # widget.currentItemChanged.connect(self.index_changed)
        # widget.currentTextChanged.connect(self.text_changed)
        widget.currentRowChanged.connect(self.row_changed)

        layout.addWidget(widget)

        b1=QPushButton("Done")
        b1.clicked.connect(lambda:self.close()) #function binded to the button self.b1
        layout.addWidget(b1)

        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

    def index_changed(self, i): # Not an index, i is a QListItem
        print(i.text())

    def row_changed(self, i):
        global selection
        selection = i
        
    def text_changed(self, s): # s is a str
        print(s)

def main():
    args = parse_args()
    app = QApplication(sys.argv)
    # this script will be launched from another python app using subprocess.check_ouput()
    # stdin will be bitstream created by launcher
    logins = sys.stdin.buffer.read().decode('utf-8').split('\n')
    w = MainWindow(args.title, args.listtype, logins)
    w.show()
    app.exec()
    # print value of selection to stdout which is the consumed by launcher
    print(f"{selection}")

if __name__ == '__main__':
    main()
